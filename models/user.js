"use strict";
const validator = require("validator");
const bcrypt = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      code: DataTypes.STRING,
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        validate: {
          email(value) {
            if (!validator.isEmail(value)) {
              throw new Error("Email is invalid");
            }
          }
        }
      },
      password: DataTypes.STRING,
      content: DataTypes.STRING,
      avatar: DataTypes.STRING,
      hasInterest: DataTypes.STRING,
      hasGoal: DataTypes.STRING,
      token: DataTypes.STRING
    },
    {}
  );
  User.associate = function(models) {
    // associations can be defined here
    User.belongsToMany(models.Category, {
      through: "user_categories",
      as: "interests",
      foreignKey: "userId"
    });
    User.belongsToMany(models.Course, {
      through: "user_reviews",
      as: "reviews",
      foreignKey: "userId"
    });
    User.belongsToMany(models.Course, {
      through: "user_courses",
      as: "userCourses",
      foreignKey: "userId"
    });
    User.belongsToMany(models.Video, {
      through: "user_videos",
      as: "watched",
      foreignKey: "userId"
    });
  };

  // Delete some data of user when send the response
  User.prototype.toJSON = function() {
    var user = Object.assign({}, this.get());

    delete user.password;

    return user;
  };

  // Check user by email and compare hash password
  User.findByCredentials = async (email, password) => {
    const user = await User.findOne({
      where: { email }
    });

    if (!user) {
      throw new Error("Unable to login");
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      throw new Error("Unable to login");
    }
    return user;
  };

  // Hash the password before register
  User.beforeCreate((user, options) => {
    return bcrypt
      .hash(user.password, 10)
      .then(hash => {
        user.password = hash;
      })
      .catch(err => {
        throw new Error();
      });
  });
  return User;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const Subcategory = sequelize.define(
    "Subcategory",
    {
      categoryId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      nameTH: DataTypes.STRING,
      icon: DataTypes.STRING,
      content: DataTypes.STRING
    },
    {}
  );
  Subcategory.associate = function(models) {
    // associations can be defined here
  };
  return Subcategory;
};

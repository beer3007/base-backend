"use strict";
module.exports = (sequelize, DataTypes) => {
  const user_category = sequelize.define(
    "user_category",
    {
      userId: DataTypes.INTEGER,
      categoryId: DataTypes.INTEGER
    },
    {}
  );
  user_category.associate = function(models) {
    // associations can be defined here
    user_category.belongsTo(models.User, { foreignKey: "userId" });
    user_category.belongsTo(models.Category, { foreignKey: "categoryId" });
  };
  return user_category;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const Video = sequelize.define(
    "Video",
    {
      courseId: DataTypes.INTEGER,
      title: DataTypes.STRING,
      order: DataTypes.INTEGER,
      thumbnail: DataTypes.STRING,
      views: DataTypes.INTEGER,
      watched: DataTypes.INTEGER,
      duration: DataTypes.INTEGER,
      quality_360: DataTypes.STRING,
      quality_480: DataTypes.STRING,
      quality_720: DataTypes.STRING
    },
    {}
  );
  Video.associate = function(models) {
    // associations can be defined here
    Video.belongsToMany(models.User, {
      through: "user_videos",
      as: "userWatched",
      foreignKey: "videoId"
    });
  };
  return Video;
};

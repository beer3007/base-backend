"use strict";
module.exports = (sequelize, DataTypes) => {
  const user_skill = sequelize.define(
    "user_skill",
    {
      userId: DataTypes.INTEGER,
      skillId: DataTypes.INTEGER,
      second: DataTypes.INTEGER
    },
    {}
  );
  user_skill.associate = function(models) {
    // associations can be defined here
  };
  return user_skill;
};

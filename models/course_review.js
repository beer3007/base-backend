"use strict";
module.exports = (sequelize, DataTypes) => {
  const course_review = sequelize.define(
    "course_review",
    {
      title: DataTypes.STRING,
      content: DataTypes.STRING,
      rating: DataTypes.DECIMAL(2,1),
      courseId: DataTypes.INTEGER,
      userId: DataTypes.INTEGER
    },
    {}
  );
  course_review.associate = function(models) {
    // associations can be defined here
    course_review.belongsTo(models.User, { foreignKey: "userId" });
    course_review.belongsTo(models.Course, { foreignKey: "courseId" });
  };
  return course_review;
};

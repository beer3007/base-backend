"use strict";
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define(
    "Category",
    {
      name: DataTypes.STRING,
      nameTH: DataTypes.STRING,
      icon: DataTypes.STRING,
      content: DataTypes.STRING
    },
    {}
  );
  Category.associate = function(models) {
    // associations can be defined here
    Category.belongsToMany(models.User, {
      through: "user_categories",
      as: "users",
      foreignKey: "categoryId",
    });
    Category.hasMany(models.Course, {
      foreignKey: "categoryId",
      as: "courses"
    })
  };
  return Category;
};

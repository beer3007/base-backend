const CourseReview = require("../models").course_review;

module.exports = {
  createCourseReview: async (req, res) => {
    try {
      const review = await CourseReview.create({
        title: req.body.title,
        content: req.body.content,
        rating: req.body.rating,
        courseId: req.params.courseId,
        userId: req.body.userId
      });
      res.send({
          status: true,
          data: review
      })
    } catch (error) {
      res.status(500).send({
        status: false,
        error: error.message
      });
    }
  }
};

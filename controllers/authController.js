const User = require("../models").User;
const { createToken } = require("../middlewares/auth");
const genarateUserCode = require('../components/genarateUserCode')

register = async (req, res) => {
    try {
        const code = await genarateUserCode();

        const user = await User.create({
            code: code,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        });

        const payload = {
            key: process.env.SECRET_KEY,
            user: user.code,
            iat: new Date().getTime()
        };

        const token = createToken(payload);

        const updateToken = await User.findOne({
            where: {
                code: user.code
            }
        });

        updateToken.update({
            token: token
        });

        res.send({
            status: 200,
            data: user,
            token: token
        });
    } catch (error) {
        res.status(400).send(error);
    }
}

login = async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        if (!user) {
            return res.send({
                status: 403,
                message: "Wrong username and password"
            });
        }

        const payload = {
            token: process.env.SECRET_KEY,
            user: user.code,
            iat: new Date().getTime()
        };

        const token = createToken(payload);

        const updateToken = await User.findOne({
            where: {
                code: user.code
            }
        });

        updateToken.update({
            token: token
        });

        res.send({
            status: 200,
            data: user,
            token: token
        });
    } catch (error) {
        res.status(404).send({ error: error.message });
    }
}

logout = async (req, res) => {
    const updateToken = await User.findOne({
        where: {
            code: req.body.code
        }
    });

    updateToken.update({
        token: null
    });

    res.send({
        status: 200,
        data: {
            message: "Logged out"
        }
    });
}

module.exports.register = register;
module.exports.login = login;
module.exports.logout = logout;

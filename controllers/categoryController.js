const Category = require("../models").Category;
const User = require("../models").User;

module.exports = {
  // get all categories
  getCategories: async (req, res) => {
    try {
      const categories = await Category.findAll();
      res.send({
        status: true,
        data: categories
      });
    } catch (error) {
      res.status(500).send({
        error: error.message
      });
    }
  }
};

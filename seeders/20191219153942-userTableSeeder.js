"use strict";
const bcrypt = require("bcryptjs");
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let users = [];
    let amount = 50;
    let password = await bcrypt.hash("secret", 10);

    users.push({
      code: "100000",
      name: "John Doe",
      email: "john@doe.com",
      password: password,
      content: "Anim aute qui eu esse minim sunt ad Lorem.",
      createdAt: new Date(),
      updatedAt: new Date()
    });

    while (amount--) {
      users.push({
        code: faker.random.number(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: password,
        content: faker.lorem.sentence(),
        createdAt: new Date(),
        updatedAt: new Date()
      });
    }
    return queryInterface.bulkInsert("users", users, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  }
};

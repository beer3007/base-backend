"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let courseSkills = [];
    let amount = 500;
    let maxSkillsPerCourse = 3;

    function randomBetween(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    for (let i = 0; i < amount; i++) {
      let amountOfCourseSkills = randomBetween(1, maxSkillsPerCourse);

      for (let n = 0; n < amountOfCourseSkills; n++) {
        let randomSkillId = randomBetween(1, 200);
        courseSkills.push({
          courseId: i + 1,
          skillId: randomSkillId,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      }
    }

    return await queryInterface.bulkInsert("course_skills", courseSkills, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("course_skills", null, {});
  }
};

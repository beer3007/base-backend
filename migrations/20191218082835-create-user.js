"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("users", {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      code: {
        type: Sequelize.STRING(10),
        allowNull: false,
        unique: true,
        primaryKey: true
      },
      email: {
        type: Sequelize.STRING(60),
        allowNull: false,
        unique: true
      },
      name: {
        type: Sequelize.STRING(60),
        allowNull: true,
        unique: true
      },
      password: {
        type: Sequelize.STRING(60),
        allowNull: false
      },
      content: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      avatar: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      hasInterest: {
        type: Sequelize.BOOLEAN
      },
      hasGoal: {
        type: Sequelize.BOOLEAN
      },
      token: {
        type: Sequelize.STRING(255),
        allowNull: true
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("users");
  }
};

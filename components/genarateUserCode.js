const User = require("../models").User;
const randomstring = require("randomstring");

genarateUserCode = async () => {
    const userCode = randomstring.generate(10);
    const checkCode = await User.findOne({
        where: {
            code: userCode
        }
    });

    if (checkCode) {
        await genarateUserCode();
    }
    // console.log(userCode);
    
    return userCode;
}

module.exports = genarateUserCode;